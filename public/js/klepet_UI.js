function divElementEnostavniTekst(sporocilo) {

  var sporociloArray = sporocilo.split(" ");
  var novo2 = '';
  var grdeBesede = ["anal","anus","arse","ass","ballsack","balls","bastard","bitch","biatch","bloody","blowjob","blow job","bollock","bollok","boner","boob","bugger","bum","butt","buttplug","clitoris","cock","coon","crap","cunt","damn","dick","dildo",
                    "dyke","fag","feck","fellate","fellatio","felching","fuck","f u c k","fudgepacker","fudge packer","flange","Goddamn","God damn","hell","homo","jerk","jizz","knobend","knob end","labia","lmao","lmfao","muff","nigger","nigga","omg",
                    "penis","piss","poop","prick","pube","pussy","queer","scrotum","sex","shit","s hit","sh1t","slut","smegma","spunk","tit","tosser","turd","twat","vagina","wank","whore","wtf"];
  
  for (var i = 0; i < sporociloArray.length; i++) {
    for (var j = 0; j < grdeBesede.length; j++) {
      if (sporociloArray[i] == grdeBesede[j]) {
        for (var k = 0; k < grdeBesede[j].length; k++) {  
          novo2 = novo2 + '*';
        }
        novo2 = novo2 + ' ';
        break;
      }
      else if (j == grdeBesede.length - 1) {
        novo2 = novo2 + sporociloArray[i] + ' ';
      }
    }
    
    if (sporociloArray.length == i + 1) {
      novo2 = novo2 + '\n';
    }
  }
  
  i = 0;
  var novo = '';

  for (i = 0; i < novo2.length; i++) {
    
    if (novo2.charAt(i) == ';' && novo2.charAt(i + 1) == ')') {
      novo = novo + '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png" class="avatar">';
      i = i + 1;
    }
    else if (novo2.charAt(i) == ':' && novo2.charAt(i + 1) == ')') {
      novo = novo + '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png" class="avatar">';
      i = i + 1;
    }
    else if (novo2.charAt(i) == '(' && novo2.charAt(i + 1) == 'y' && novo2.charAt(i + 2) == ')') {
      novo = novo + '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png" class="avatar">';
      i = i + 2;
    }
    else if (novo2.charAt(i) == ':' && novo2.charAt(i + 1) == '*') {
      novo = novo + '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png" class="avatar">';
      i = i + 1;
    }
    else if (novo2.charAt(i) == ':' && novo2.charAt(i + 1) == '(') {
      novo = novo + '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png" class="avatar">';
      i = i + 1;
    }
    else {
      novo = novo + novo2.charAt(i);
    }
    
  }
  return $('<div style="font-weight: bold"></div>').html(novo);
 
}

String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  var vzdevek1 = ' ';
  var kanal1 = ' ';

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      vzdevek1 = rezultat.vzdevek;
      sporocilo = 'Prijavljen si kot ' + vzdevek1 + '.';
            $('#kanal').text(vzdevek1 + ' @ ' + kanal1);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    kanal1 = rezultat.kanal;
    $('#kanal').text(vzdevek1 + ' @ ' + kanal1);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});